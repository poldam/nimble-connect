<?php
/**
 * Plugin Name: Nimble Connect Plugin
 * Plugin URI: http://estros.gr
 * Description: This plugin adds a popup form through a shortcode that enables to register records straight to your Nimble account.
 * Version: 1.0
 * Author: Estros.gr
 * Author URI: http://estros.gr
 * License: GPL2
 */

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

require_once('api_nimble.php');

function enform($atts, $content){
  $buttontext = "Sign me up!";
  if(!empty($content))
    $buttontext = $content;
  extract( shortcode_atts( array(
    'class' => 'en-button',
		'popup' => '1',
		'id' => '0',
  ), $atts, 'estros-nimble' ) );
$parent = "#applicationForm".$id;
?>
  <script type="text/javascript">
		jQuery(document).ready(function () { 
		//alert(jQuery('<?php echo $parent; ?>').html());
		 
			<?php if($popup == 1 ) { ?>
			jQuery('#launchapp<?php echo $id; ?>').on('click', function () {
				jQuery('<?php echo $parent; ?>').show();
				jQuery('#cover<?php echo $id; ?>').show();
				jQuery('html, body').animate({ scrollTop: jQuery('#applicationForm<?php echo $id; ?>').offset().top - 100 }, 'slow');
			});
			
			jQuery('#enFormClose<?php echo $id; ?>').on('click', function () {
				jQuery('<?php echo $parent; ?>').hide();
				jQuery('#cover<?php echo $id; ?>').hide();
			});
			<?php } ?>
			<?php if($popup == 0 ) { ?>
				jQuery('<?php echo $parent; ?>').show();
			<?php } ?>
		});
  </script>
  <?php 
	include 'frontend-form.php'; 
	if($popup == 1 ) 
		$returndata .= "<button id=\"launchapp".$id."\" class=\"".$class."\">".$buttontext."</button>";
	return $returndata;
}

function wptuts_scripts_important()
{
  wp_enqueue_script('jquery');
  wp_register_style( 'estrosNimbleStyle', plugins_url( '/css/estros-nimble.css', __FILE__ ), array(), '20120208', 'all' );
  wp_enqueue_style( 'estrosNimbleStyle' );
}
add_action('admin_menu', 'estrosnimble_admin');
add_action( 'admin_enqueue_scripts', 'en_load_admin_style' );

function en_load_admin_style() {
  wp_register_style( 'admin_css', plugins_url( '/css/estros-nimble.css', __FILE__ ), false, '1.0.0' );
  wp_enqueue_script('jquery');
 }
 
function estrosnimble_admin(){
  add_menu_page( 'Nimble Integration', 'My Nimble', 'administrator', 'estros-nimble', 'estrosnimble_init', 'dashicons-id' );
}
 
function estrosnimble_init(){
  ?>
  <div class="wrap">
    <div style="padding:5px; display: inline-block; width:42%;">   
      <h2>Nimble Integration Plugin</h2>
      <div class="welcome-panel">
        <a href="https://estros.gr" target="_blank">
          <img src="http://estros.gr/logo.png" height="45">
        </a>
        <div style="width:100%; font-size: 18px; font-weight: bold;">Plugin Description</div>
        <p>This plugin adds a popup/form through a shortcode that enables to register records straight to your Nimble account.</p>
      </div>
     
      <div class="welcome-panel">
        <h3>Nimble API Settings</h3>
        <?php
        if($_GET['settings-updated'])
        {
        ?>
          <div style="font-size: 12px; border: 2px #1EA1CC solid;padding:2px;background-color: #2EA2CC; color: white;">Your settings have been saved</div><br>
        <?php
        }
        ?>
        <?php include 'en-admin-api-settings.php'; ?>
      </div>
  	</div>
    <div class="welcome-panel" style="vertical-align: top; width: 53%; display: inline-block;">
      <h2>How to use the shortcode</h2>
      <p>Place this shortcode in any post/page/product description to get the registration popup/form. Using this plugin visitors of your site will be able to insert their information straight to your nimble account through the plugin form!</p>
      <p>
        <code>
        	[estros-nimble class="mybuttonclass" popup=1]Your Button Text Here[/estros-nimble]
        </code>
        <p>
        	If attribute 'popup' is set to 0 then the popup functionality is disabled and the form appears straight to the container where the shortcode is put.<br />
          If popup is ommited then the default behavior is adding a popup button.
        </p>
      </p>
      <p>
      	<h3>How to set up the plugin</h3>
      	To use this plugin you need to get your API keys<br />
        For more info go <a href="http://support.nimble.com/customer/portal/articles/1194074-nimble-api-access" target="_blank">here</a>!
      </p>
      <p>
      	<h3>Tags</h3>
      	Currently only 5 tags can be passed on though the Nimble API.<br />
        The frontend form allows the selection of up to 5 checkboxes based on the tags<br />
        you have defined in the tags input on the left (i.e. CRM, PHP, InternetMarketing).<br />
        However you can define as many tags as you like.
      </p>
      <p>
      	<h3>Custom Fields</h3>
        Use the following format:<br />
      	i.e. Github:text:Github,Stackoverflow:text:Stackoverflow,Remotely:checkbox:Want to work remotely.<br />
        (Explanation:  CustomFieldName:inputType:inputLabel)<br />
        So far only text and checkbox are supported.<br />
        Make sure you name these fields using exactly the same names you used in your Nimble Account.<br />
        For enabling the Newsletter option you need to create a custom field named 'Newsletter' first!<br />
        To create a custom field: Login to Nimble > "Username" > Settings > Datafields Tab. The menu is on the top right corner!<br />
        The number of the custom fields depends on the restrictions of your Nimble Account.
      </p>
      <p>
      	<h3>Companies</h3>
        You can select the contact type to be a company!<br />
        In this case the plugin adds the person who fills the form as a contact and creates the Company, based on the company name given.<br />
        A relationship between the person and the company is also created. If the company already exists then its is not created again.<br />
        Additionally an activity task is created reminding you to contact that person/company. <br />
        In 'company' mode there is no 'job title', linkedin, skypeid, facebook etc. and there is an additional field named 'Message' for the company to describe the reason of the request if needed.
      </p>
      <p>
      	<h3>Newsletter</h3>
        For Newsletter option to work you need to have a custom field in your account named 'Newsletter'
      </p>
  	</div>
  </div>
  <?php
}
add_action( 'admin_init', 'en_plugin_settings' );
function en_plugin_settings() {
  register_setting( 'en-plugin-settings-group', 'nimble_access_token' );
  register_setting( 'en-plugin-settings-group', 'nimble_refresh_token' );
	register_setting( 'en-plugin-settings-group', 'nimble_client_id' );
  register_setting( 'en-plugin-settings-group', 'nimble_client_secret' );
	register_setting( 'en-plugin-settings-group', 'nimble_redirect_uri' );
	register_setting( 'en-plugin-settings-group', 'nimble_tags' );
	register_setting( 'en-plugin-settings-group', 'nimble_tags_title' );
	register_setting( 'en-plugin-settings-group', 'nimble_customfields' );
	register_setting( 'en-plugin-settings-group', 'nimble_newsletter' );
	register_setting( 'en-plugin-settings-group', 'nimble_newsletter_title' );
	register_setting( 'en-plugin-settings-group', 'nimble_contact_type' );
	register_setting( 'en-plugin-settings-group', 'nimble_form_title' );
	register_setting( 'en-plugin-settings-group', 'nimble_thanx' );
}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_important', 5 );
add_shortcode('estros-nimble', 'enform');