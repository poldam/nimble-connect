# estros-nimble #
## How to use the shortcode ##
Place this shortcode in any post/page/product description to get the registration popup/form. Using this plugin visitors of your site will be able to insert their information straight to your nimble account through the plugin form!

[estros-nimble class="mybuttonclass" popup=1]Your Button Text Here[/estros-nimble]

If attribute 'popup' is set to 0 then the popup functionality is disabled and the form appears straight to the container where the shortcode is put.
If popup is ommited then the default behavior is adding a popup button.

## How to set up the plugin ##
To use this plugin you need to get your API keys
For more info go here!

## Tags ##
Currently only 5 tags can be passed on though the Nimble API.
The frontend form allows the selection of up to 5 checkboxes based on the tags
you have defined in the tags input on the left (i.e. CRM, PHP, InternetMarketing).
However you can define as many tags as you like.

## Custom Fields ##
Use the following format:

i.e. Github:text:Github,Stackoverflow:text:Stackoverflow,Remotely:checkbox:Want to work remotely.
(Explanation: CustomFieldName:inputType:inputLabel)

So far only text and checkbox are supported.

Make sure you name these fields using exactly the same names you used in your Nimble Account.

For enabling the Newsletter option you need to create a custom field named 'Newsletter' first!

To create a custom field: Login to Nimble > "Username" > Settings > Datafields Tab. The menu is on the top right corner!

The number of the custom fields depends on the restrictions of your Nimble Account.

## Companies ##
You can select the contact type to be a company!

In this case the plugin adds the person who fills the form as a contact and creates the Company, based on the company name given.

A relationship between the person and the company is also created. If the company already exists then its is not created again.

Additionally an activity task is created reminding you to contact that person/company.

In 'company' mode there is no 'job title', linkedin, skypeid, facebook etc. and there is an additional field named 'Message' for the company to describe the reason of the request if needed.

## Newsletter ##
For Newsletter option to work you need to have a custom field in your account named 'Newsletter'