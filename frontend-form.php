<?php 
$parent = "#applicationForm".$id;
$returndata = "
<script type=\"application/javascript\">
	jQuery(function(){
		
		var max = 5;
		
		var checkboxes = jQuery('input[name=\"itskills".$id."[]\"]');
		checkboxes.change(function(){
			var current = checkboxes.filter(':checked').length;
			checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
		});
		
		jQuery('#firstname".$id."').on('change', function(){
			checkFirstname".$id."();
		});
		
		jQuery('#lastname".$id."').on('change', function(){
			checkLastname".$id."();
		});";
		
		if(esc_attr(get_option('nimble_contact_type')) == 1) {
			$returndata .= "
      jQuery('#companyname".$id."').on('change', function(){
        checkCompanyname".$id."();
      });";      
		} 
		$returndata .= "
		jQuery('#emailaddress".$id."').on('change', function(){
			checkEmail".$id."();
		});
		
	});";
	
	if($popup == 1 ) 
	{ 
		$returndata .= "
		jQuery(document).mouseup(function (e)
		{
			var container = jQuery('#applicationForm".$id."');
			if (!container.is(e.target) && container.has(e.target).length === 0)
			{
				jQuery('#applicationForm".$id."').hide();
				jQuery('#cover".$id."').hide();
			}
		});";
	} 
	
	$returndata .= "
	function isValidEmailAddress".$id."(emailAddress) {
		var pattern = new RegExp(/^((\"[\w-+\s]+\")|([\w-+]+(?:\.[\w-+]+)*)|(\"[\w-+\s]+\")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
		return pattern.test(emailAddress);
	};
	
	function checkFirstname".$id."()
	{
		if(jQuery('#firstname".$id."').val().length < 3)
			jQuery('#firstname".$id."').addClass('enError');
		else
			jQuery('#firstname".$id."').removeClass('enError');
	}
	
	function checkCompanyname".$id."()
	{
		if(jQuery('#companyname".$id."').val().length < 1)
			jQuery('#companyname".$id."').addClass('enError');
		else
			jQuery('#companyname".$id."').removeClass('enError');
	}
	
	function checkLastname".$id."()
	{
		if(jQuery('#lastname".$id."').val().length < 3)
			jQuery('#lastname".$id."').addClass('enError');
		else
			jQuery('#lastname".$id."').removeClass('enError');
	}
	
	function checkEmail".$id."()
	{
		if(!isValidEmailAddress".$id."(jQuery('#emailaddress".$id."').val()))
			jQuery('#emailaddress".$id."').addClass('enError');
		else
			jQuery('#emailaddress".$id."').removeClass('enError');
	}
	
	function submitForm".$id."() {
		var errors = '';
		
		var emailaddress = jQuery('#emailaddress".$id."').val();
		
		var tags = new Array();
		jQuery.each(jQuery(\"input[name='itskills".$id."[]']:checked\"), function() {
			tags.push(jQuery(this).val());
		});
		
		var firstname = jQuery('#firstname".$id."').val();
		var lastname = jQuery('#lastname".$id."').val();
		if(firstname.length < 3)
		{
			errors += \"Firstname must be at least 3 characters.\\n\";
			jQuery('#firstname".$id."').addClass('enError');
		}
		if(lastname.length < 3)
		{
			errors += \"Lastname must be at least 3 characters.\\n\";
			jQuery('#lastname".$id."').addClass('enError');
		}";
		
		if(esc_attr(get_option('nimble_contact_type')) == 1) 
		{
			$returndata .= "
			var companyname = jQuery('#companyname".$id."').val();
			var message = jQuery('#message".$id."').val();
			if(companyname.length < 1)
			{
				errors += \"Company name can\'t be empty.\\n\";
				jQuery('#companyname".$id."').addClass('enError');
			}";
		} 
		
		$returndata .= "
		if(!isValidEmailAddress".$id."(emailaddress))
		{
			errors += \"Email address must be valid (myemail@email.com).\\n\";
			jQuery('#emailaddress".$id."').addClass('enError');
		}
		
		if(errors == '')
		{
			jQuery('#applicationForm".$id."').hide();
			jQuery('#cover".$id."').hide();
			jQuery('#enspinner".$id."').show();
			var newsletter = 'No';
			
			if(jQuery('#nimble_newsletter".$id."').prop('checked'))
				newsletter = 'Yes';
				";
				
			
			$customfields = str_replace(' ', '', esc_attr(get_option('nimble_customfields')));
			$customfields = explode(',', $customfields);
			
			for($i = 0; $i < sizeof($customfields); $i++)
			{
				$tempfields = explode(":", $customfields[$i]);
				if($tempfields[0] != "") 
				{
					$returndata .=  "var ".str_replace(' ', '', $tempfields[0]).";";
					if($tempfields[1] == "text") 
					{ 
						$returndata .= trim($tempfields[0])." = jQuery('#".str_replace(' ', '', $tempfields[0])."".$id."').val();";
					}
					else
					{
						$returndata .= "if(jQuery('#".str_replace(' ', '', $tempfields[0])."".$id."').prop('checked')) {";
						$returndata .= str_replace(' ', '', $tempfields[0])." = 'Yes'; }";
						$returndata .= "else {";
						$returndata .= str_replace(' ', '', $tempfields[0])." = 'No'; }";
					}
				}
			}
			
			$returndata .= "
			jQuery.ajax({
				url: \"".plugin_dir_url( __FILE__ )."addRecord.php\",
				dataType : 'HTML',
				method: 'POST',
				data: { 
						firstname: firstname,
						lastname: lastname,";
					if(esc_attr(get_option('nimble_contact_type')) == 0) {
						$returndata .= "title: jQuery('#title".$id."').val(),";
					} else { 
						$returndata .= "companyname: companyname,
						message: message,";
					} 
					$returndata .= "
					emailaddress: emailaddress,
					phone_work: jQuery('#phone_work".$id."').val(),";
					if(esc_attr(get_option('nimble_contact_type')) == 0) {
						$returndata .= "
						phone_mobile: jQuery('#phone_mobile".$id."').val(),
						skypeid: jQuery('#skypeid".$id."').val(),
						facebook: jQuery('#facebook".$id."').val(),
						linkedin: jQuery('#linkedin".$id."').val(),";
					}
					$returndata .= "
					tags: tags.toString(),
					token: jQuery('#token".$id."').val(),
					nimble_customs: '".str_replace(' ', '', esc_attr(get_option('nimble_customfields')))."',
					newsletter: newsletter";
					for($i = 0; $i < sizeof($customfields); $i++)
					{
						$tempfields = explode(":", $customfields[$i]);
						if($tempfields[0] != "") 
						{
							$returndata .= ",".trim($tempfields[0]).": ".trim($tempfields[0]);
						}
					} 
					$returndata .= "
					},
				success: function(result){
					if(result == 201)
					{
						jQuery('#applicationForm".$id."').hide();
						jQuery('#cover".$id."').hide();
						jQuery('#enspinner".$id."').hide();
						alert(\"".esc_attr(get_option('nimble_thanx'))."\");
					}
					else
					{
						jQuery('#enspinner".$id."').hide();
						jQuery('#applicationForm".$id."').show();";
						if($popup == 1 ) 
						{ 
							$returndata .= "jQuery('#cover".$id."').show();
							jQuery('html, body').animate({ scrollTop: jQuery('#applicationForm".$id."').offset().top - 100 }, 'slow');";
						}
						$returndata .= "alert(\"Service is currently unavailable..\\nPlease try again later!\");
					}
				},
				error: function(result){
					alert(\"Error: Service is currently unavailable..\\nPlease try again later!\");
					jQuery('#applicationForm".$id."').show();";
					if($popup == 1 ) 
					{ 
						$returndata .= "jQuery('#cover".$id."').show();
						jQuery('html, body').animate({ scrollTop: jQuery('#applicationForm".$id."').offset().top - 100 }, 'slow');";
					} 
					$returndata .= "jQuery('#enspinner".$id."').hide();
				}
			});
		}
		else
			alert(errors);
	}
</script>";

$returndata .= '
<div id="cover'.$id.'"> </div>
<div id="enspinner'.$id.'"> </div>

<form id="applicationForm'.$id.'" class="enFormloader" onSubmit="return false;" method="post" name="Application'.$id.'">
	
  <div style="text-align: right; margin-top: -5px;">';
    if($popup == 1 ) { 
      $returndata .= '<span id="enFormClose'.$id.'"><img src="'.plugin_dir_url( __FILE__ ).'images/close.png" width="25"></span>';
    } else { $returndata .= "<div style='height: 25px;'></div>"; }
	$returndata .= '
  </div>
 	
  <div class="appformTitle">
    '.esc_attr(get_option('nimble_form_title')).'
  </div>

  <div class="inlineblock">
    <div>First name</div>
    <div> <input type="text" name="firstname'.$id.'" id="firstname'.$id.'" class="entextfield" ></div>
  </div>
  <div class="inlineblock">
    <div>Last name</div>
    <div> <input type="text" name="lastname'.$id.'" id="lastname'.$id.'" class="entextfield" ></div>
  </div>';
	
  if(esc_attr(get_option('nimble_contact_type')) == 1) {
		$returndata .= '
    <div class="inlineblock" style="width: 98%;">
      <div>Company Name</div>
      <div> <input type="text" name="companyname'.$id.'" id="companyname'.$id.'" class="entextfield" ></div>
    </div>';
  } 
	$returndata .= '
  <div class="inlineblock">
    <div>Email Address</div>
    <div> <input type="text" name="emailaddress'.$id.'" id="emailaddress'.$id.'" class="entextfield" ></div>
  </div>';
  if(esc_attr(get_option('nimble_contact_type')) == 0) { 
    $returndata .= '
		<div class="inlineblock">
      <div>Job Title</div>
      <div> <input type="text" name="title'.$id.'" id="title'.$id.'" class="entextfield"></div>
    </div>';
  } 
	$returndata .= '
  <div class="inlineblock">
    <div>Phone </div>
    <div> <input type="text" name="phone_work'.$id.'" id="phone_work'.$id.'" class="entextfield"></div>
  </div>';
  if(esc_attr(get_option('nimble_contact_type')) == 0) {
		$returndata .= '
    <div class="inlineblock">
      <div>Mobile Phone</div>
      <div> <input type="text" name="phone_mobile'.$id.'" id="phone_mobile'.$id.'" class="entextfield"></div>
    </div>
    <div class="inlineblock">
      <div>Skype id</div>
      <div> <input type="text" name="skypeid'.$id.'" id="skypeid'.$id.'" class="entextfield"></div>
    </div>
    <div class="inlineblock">
      <div>Facebook</div>
      <div> <input type="text" name="facebook'.$id.'" id="facebook'.$id.'" class="entextfield"></div>
    </div>
    <div class="inlineblock">
      <div>Linkedin</div>
      <div> <input type="text" name="linkedin'.$id.'" id="linkedin'.$id.'" class="entextfield"></div>
    </div>';
  } 
  
  if(esc_attr(get_option('nimble_contact_type')) == 1) { 
	$returndata .= '
    <div class="inlineblock" style="width: 98%;">
    <div>How can we help you?</div>
      <textarea name="message'.$id.'" id="message'.$id.'" class="entextfield" style="height:100px;"></textarea>
    </div>';
   } 
  
  
		$customfields = trim(esc_attr(get_option('nimble_customfields')));
		$customfields = explode(',', $customfields);
		
		for($i = 0; $i < sizeof($customfields); $i++)
		{
			$tempfields = explode(":", $customfields[$i]);
			if($tempfields[0] != "") 
			{
				if($tempfields[1] == "text") 
				{ 
					$returndata .= ' 
					<div class="inlineblock">
						<div>'.$tempfields[2].'</div>
						<div> <input type="text" name="'.trim($tempfields[0]).''.$id.'" id="'.trim($tempfields[0]).''.$id.'" class="entextfield"></div>
					</div>'; 
				} 
				else 
				{
					$returndata .= ' 
					<div class="inlineblock">
						<input type="checkbox" name="'.trim($tempfields[0]).''.$id.'" id="'.trim($tempfields[0]).''.$id.'" style="margin-top:17px;" > '.$tempfields[2].'
					</div>';
				}
			}
		}

		$tags_title = esc_attr(get_option('nimble_tags_title'));
		$tags = explode(",", esc_attr(get_option('nimble_tags')));

		if($tags_title != "")
			$returndata .= '<p style="width: 100%; text-align:center; margin-bottom:0px; padding-top:5px; font-weight:bold;">'.$tags_title.'</p>';

		if( esc_attr(get_option('nimble_tags')) != "")
		{
			$returndata .= '<div class="inlineblock" style="width: 30%;">';
			$howmany = sizeof($tags);
			$howmany = intval($howmany/3);
			
			$j = 0;
			for($i = 0; $i < sizeof($tags); $i++)
			{
				if($j == $howmany)
				{
					$returndata .= "</div><div class=\"inlineblock\" style=\"width: 30%;\">";
					$j = 0;
				}
				$returndata .= '<input type="checkbox" name="itskills'.$id.'[]" value="'.$tags[$i].'"/> '.$tags[$i].'<br />'; 
				$j++;
			}
			$returndata .= '</div>';
		}

		$newsletter = esc_attr(get_option('nimble_newsletter'));
		$nimble_newsletter_title = esc_attr(get_option('nimble_newsletter_title'));
		if($newsletter != "" && $newsletter == "Yes")
		{
			$returndata .= '
			<div style="width: 100%; text-align:center; padding:5px; padding-top:15px; font-weight:bold;">
				<input type="checkbox" name="nimble_newsletter'.$id.'" id="nimble_newsletter'.$id.'" checked>'.$nimble_newsletter_title.'
			</div>';
		}

$returndata .= '
  <input id="token'.$id.'" name="token'.$id.'" type="hidden" value="u7y3cohysyiqDT3y9t1hVp32szhIsXlXdW7HTFh1" />
  <input id="contact_type'.$id.'" name="contact_type'.$id.'" type="hidden" value="'.esc_attr(get_option('nimble_contact_type')).'" />
  <div style="width: 100%; text-align: center; margin-top:10px;">
    <input id="buttonSubmit'.$id.'" onClick="submitForm'.$id.'();" type="submit" value="'.$buttontext.'" class="'.$class.'"/>
  </div>
</form>';

?>