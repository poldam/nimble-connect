<?php

class NimbleAPI
{

  public function __construct()
  {
    $this->config = array(
      'api_key' => get_option('nimble_client_id'),
      'secret_key' => get_option('nimble_client_secret'),
      'redirect_uri' => get_option('nimble_redirect_uri')
    );
  }
 
	public function nimble_get_auth_code()
	{
		$uri = 'https://api.nimble.com/oauth/authorize?client_id='.$this->config['api_key'].'&redirect_uri='.$this->config['redirect_uri'].'&response_type=code';
		header("Location: $uri");
	}
	 
	public function nimble_get_access_token($code)
	{
		$url = 'https://api.nimble.com/oauth/token';
		$method = 'POST';
		$data = 'client_id='.$this->config['api_key'].'&client_secret='.$this->config['secret_key'].'&code=' . $code . '&grant_type=authorization_code&redirect_uri='.$this->config['redirect_uri'];
		$headers = array(
			'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'
		);
		$response_data = $this->nimble_request($url, $method, $data, $headers);
		return $response_data;
	}
	
	public function nimble_refreshtoken_get_access_token()
	{
		$request_token = get_option('nimble_refresh_token');
		$url = 'https://api.nimble.com/oauth/token';
		$method = 'POST';
		$headers = array(
			'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'
		);
		$data = 'client_id='.$this->config['api_key'].'&client_secret='.$this->config['secret_key'].'&refresh_token=' . $request_token . '&grant_type=refresh_token&redirect_uri='.$this->config['redirect_uri'];
		$response_data = $this->nimble_request($url, $method, $data, $headers);
		return $response_data[1]->access_token;
	}
	
	public function nimble_add_person($firstname, $lastname, $emailaddress, $phone_work, $phone_mobile, $title, $skypeid, $facebook, $linkedin, $customfields, $tags, $company)
	{ 
		$access_token =  esc_attr(get_option('nimble_access_token'));
		$url    = 'https://api.nimble.com/api/v1/contact?access_token=' . $access_token; 
		$method = 'POST';
		$data = '
		{
			"fields": {
				"first name": [{
					"value": "'.$firstname.'",
					"modifier": ""
				}],
				"last name": [{
					"value": "'.$lastname.'",
					"modifier": ""
				}],
				"email": [{
					"value": "'.$emailaddress.'",
					"modifier": "personal"
				}],
				"title": [{
					"value": "'.$title.'",
					"modifier": ""
				}],
				"skype id": [{
					"value": "'.$skypeid.'",
					"modifier": ""
				}],
				"facebook": [{
					"value": "'.$facebook.'",
					"modifier": ""
				}],
				"linkedin": [{
					"value": "'.$linkedin.'",
					"modifier": ""
				}],
				"phone": [{
					"modifier": "work",
					"value": "'.$phone_work.'"
				}, {
					"modifier": "mobile",
					"value": "'.$phone_mobile.'"
				}]';
				
		if($company != "")
		{
			$data .= ',
			"parent company": [{
				"value": "'.$company.'",
				"modifier": ""
			}]';
		}
				
		for($i = 0; $i < sizeof($customfields); $i++)
		{
			if($i == 0)
				$data .= ",";
			$data .= '
			"'.$customfields[$i][0].'": [{
				"value": "'.$customfields[$i][1].'",
				"modifier": ""
			}]
			';
			if($i < (sizeof($customfields) - 1))
				$data .= ",";
		}
		
		$data .= '
			},
			"record_type": "person",
			"tags":"'.$tags.'"
		}';
		
		$headers = array(
        'Accept: application/json',
        'Content-Type: application/json'
    );
		
		$response_data = $this->nimble_request($url, $method, $data, $headers);		
		
		if ($response_data[0] == 401) 
		{
			$access_token = $this->nimble_refreshtoken_get_access_token();
			update_option('nimble_access_token', $access_token);
			$url    = 'https://api.nimble.com/api/v1/contact?access_token=' . $access_token;
			$response_data = $this->nimble_request($url, $method, $data, $headers);
		} 
		//var_dump($response_data); 
		return $response_data;		
	}
	
	public function nimble_add_task($subject, $notes, $id)
	{ 
		$access_token =  esc_attr(get_option('nimble_access_token')); 
		$url    = 'https://api.nimble.com/api/v1/activities/task?access_token=' . $access_token; 
		$method = 'POST';
		$data = '
		{
			"due_date": "2014-01-01 13:50",
			"notes": "'.$notes.'",
			"related_to": [
				"'.$id.'"
			],
			"subject": "'.$subject.'"
		}';
		
		$headers = array(
			'Accept: application/json',
			'Content-Type: application/json'
    );
		$response_data = $this->nimble_request($url, $method, $data, $headers);		
		if ($response_data[0] == 401) 
		{
			$access_token = $this->nimble_refreshtoken_get_access_token();
			update_option('nimble_access_token', $access_token);
			$url    = 'https://api.nimble.com/api/v1/contact?access_token=' . $access_token;
			$response_data = $this->nimble_request($url, $method, $data, $headers);
		} 
		//echo $data;
		//var_dump($response_data); 
		return $response_data;		
	}
	
	public function nimble_request($url, $method, $data, $headers)
	{    
		$handle = curl_init();
		
		curl_setopt($handle, CURLOPT_URL, $url);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_HEADER, true);
		curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($handle, CURLOPT_VERBOSE, FALSE);
		curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($handle, CURLOPT_TIMEOUT, 90);

		switch ($method) {	
			case 'GET':
					break;
			case 'POST':
					curl_setopt($handle, CURLOPT_POST, true);
					curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
					break;
			case 'PUT':
					curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
					curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
					break;
			case 'DELETE':
					curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
					break;
		}
		
		$response = curl_exec($handle);
		$code     = curl_getinfo($handle, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($handle, CURLINFO_HEADER_SIZE);
		$body        = substr($response, $header_size);
		$error       = json_decode($body);
		$response_data    = Array();
		$response_data[0] = $code;
		$response_data[1] = $error;
		return $response_data;
	}
}
?>