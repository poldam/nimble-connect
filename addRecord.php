<?php

if(isset($_POST['token']) && $_POST['token'] == "u7y3cohysyiqDT3y9t1hVp32szhIsXlXdW7HTFh1")
{
	require_once('../../../wp-config.php');
	
	require_once('api_nimble.php');
	
	$nimble = new NimbleAPI();
	
	$errors = 0;
	
	if(get_option('nimble_contact_type') == 1)
	{
		$companyname = $_POST['companyname'];
		$message = $_POST['message'];
	}
	
	$emailaddress = $_POST['emailaddress'];
	$phone_work = $_POST['phone_work'] ? $_POST['phone_work'] : '';
	
	if(get_option('nimble_contact_type') == 0)
	{
		$phone_mobile = $_POST['phone_mobile'] ? $_POST['phone_mobile'] : '';
		$skypeid = $_POST['skypeid'] ? $_POST['skypeid'] : '';
		$facebook = $_POST['facebook'] ? $_POST['facebook'] : '';
		$linkedin = $_POST['linkedin'] ? $_POST['linkedin'] : '';
	}
	$nimble_customs = explode(",", $_POST['nimble_customs']);

	$firstname = $_POST['firstname']; 
	$lastname = $_POST['lastname'];
	$title = $_POST['title'] ? $_POST['title'] : '';
	
	if(strlen($firstname) < 3)
	{
		$emailErr = "Firstname must be at least 3 characters.\n";
		$errors++;
	}
	
	if(strlen($lastname) < 3)
	{
		$emailErr = "Lastname must be at least 3 characters.\n";
		$errors++;
	}

	$tags = $_POST['tags'];
	
	$finalcustomfields = array();
	
	for($i = 0; $i < sizeof($nimble_customs); $i++)
	{
		$tempfields = explode(":", $nimble_customs[$i]);
		if($tempfields[0] != "") 
		{
			$finalcustomfields[] = array(trim($tempfields[0]), $_POST[$tempfields[0]]);
			$index = trim($tempfields[0]);
		}
	} 
	$finalcustomfields[] = array("Newsletter", $_POST['newsletter']);
	
	if (!filter_var($emailaddress, FILTER_VALIDATE_EMAIL)) 
	{
		$emailErr = "Invalid email format.\n";
		$errors++;
	}
	
	if(get_option('nimble_contact_type') == 0)
	{
		$title = $_POST['title'] ? $_POST['title'] : '';
	}
	
	if($errors == 0)
	{
		$tags = str_replace("+", "Plus", $tags);
		$tags = str_replace("#", "Sharp", $tags);
		
		if(get_option('nimble_contact_type') == 1)
		{
			$companyname = $_POST['companyname'];
			$response_data = $nimble->nimble_add_person($firstname, $lastname, $emailaddress, $phone_work, "", "", "", "", "", $finalcustomfields, $tags, $companyname);
			$response_data = $nimble->nimble_add_task("".date("l d.m.Y")." - Request for a Call from ".$firstname." ".$lastname." on behalf of ".$companyname, $message, $response_data[1]->id);
		}
		else
		{
			$response_data = $nimble->nimble_add_person($firstname, $lastname, $emailaddress, $phone_work, $phone_mobile, $title, $skypeid, $facebook, $linkedin, $finalcustomfields, $tags, "");
		}

		echo "\n".$response_data[0];
	}
	
	//var_dump($_POST);
}
else
{
	echo "Indirect access of the form is not allowed!";
}
?>