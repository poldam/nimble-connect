<?php
require_once('api_nimble.php');
$nimble = new NimbleAPI();
if (isset($_POST['nimble_reset']))
{
	update_option('nimble_access_token','');
	update_option('nimble_form_title','');
	update_option('nimble_refresh_token','');
	update_option('nimble_client_id', ''); 
	update_option('nimble_client_secret', '');	
	update_option('nimble_redirect_uri', '');		
	
	update_option('nimble_contact_type', 0); //0 is person, 1 is company
	update_option('nimble_tags', '');
	update_option('nimble_tags_title', 'Tags Title');
	update_option('nimble_thanx', 'Thank you for your message!');
	update_option('nimble_customfields', ''); 
	update_option('nimble_newsletter', 'No');		
	update_option('nimble_newsletter_title', 'Sign up for our Newsletter to get job offers!');
}

if (isset($_POST['nimble_prefs']))
{
	update_option('nimble_form_title',$_POST['nimble_form_title']);
	update_option('nimble_thanx',$_POST['nimble_thanx']);
	update_option('nimble_contact_type', $_POST['nimble_contact_type']);
	update_option('nimble_tags', $_POST['nimble_tags']);
	update_option('nimble_tags_title', $_POST['nimble_tags_title']);
	update_option('nimble_customfields', $_POST['nimble_customfields']); 
	update_option('nimble_newsletter', $_POST['nimble_newsletter']);		
	update_option('nimble_newsletter_title', $_POST['nimble_newsletter_title']);	
}

$flag = 1;
if ((isset($_POST['client_id'])) && (isset($_POST['client_secret'])) && (isset($_POST['redirect_uri']))) 
{
	$handle = 'https://api.nimble.com/oauth/authorize?client_id=' . $_POST["client_id"] . '&redirect_uri=' . $_POST["redirect_uri"] . '&response_type=code';
	echo '<script type="text/javascript">  window.location = "' . $handle . '"  </script>';
	$client_id = $_POST['client_id'];  
	update_option('nimble_client_id', $client_id);  
	$client_secret = $_POST['client_secret'];	
	update_option('nimble_client_secret',$client_secret);
	$redirect_uri = $_POST['redirect_uri'];
	update_option('nimble_redirect_uri',$redirect_uri);
} 
else if (isset($_GET["code"])) 
{
	$response_data = $nimble->nimble_get_access_token($_GET["code"]);
	
	if ($response_data[0] == 200) 
	{
		update_option('nimble_access_token',$response_data[1]->access_token);
		update_option('nimble_refresh_token',$response_data[1]->refresh_token);
		echo "<div class='alert alert-success'>Nimble API settings have been successfully saved.</div>";
		$flag = 0;
	} 
	else 
		echo "<div class='alert alert-error'>Error  = " . $response_data[1]->msg . "<br>". $response_data[1]->error ."</div>"; 
}
?>
<?php

if ($flag == 1) 
{
	if ((get_option('nimble_access_token')== '')&&(get_option('nimble_refresh_token') == '')) 
	{
		?>
		<br />
		<form name="nimbleform" method="POST" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">		
			<strong>API Key:</strong><br />
      <input type="text" class="span4" onblur="if(value=='') value = 'API Key'" onfocus="if(value!='') value = ''" value="API Key" id="client_id" name="client_id" ><br />
			<strong>API Secret:</strong><br />
      <input type="text" class="span4" onblur="if(value=='') value = 'API Secret'" onfocus="if(value!='') value = ''" value="API Secret" id="client_secret" name="client_secret" ><br />
			<strong>Redirect URI:</strong><br />
      <input type="text" class="span4" onblur="if(value=='') value = 'Redirect URI'"  value="<?php echo  "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] ?>" id="redirect_uri" name="redirect_uri"><br />
			<input type="hidden" name="bcpl_hidden" value="Y">  
			<?php submit_button("Get Access Token"); ?>
		</form>
		<?php
	}
	else
	{
		echo "
			<div class='enSuccess'>
				Nimble API settings have been sucessfully saved. 
			</div>";
		?>
    <form name="nimbleform2" method="POST" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">
      <input type="hidden" name="nimble_reset" value="Y"> 
      <?php submit_button("Reset Settings"); ?>
      <div class="enError" style="text-align: left;">
      Resetting settings will disconnect plugin from your Nimble account.
      </div> 
    </form>
    <?php
	}
}
?>
  <hr />
<h3>Nimble Form Preferences</h3>
<form name="nimbleform3" method="POST" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">
  <strong>Form Title:</strong><br />
  <input type="text" class="span4" value="<?php echo esc_attr( get_option('nimble_form_title') ); ?>" id="nimble_form_title" name="nimble_form_title" ><br />
  
  <strong>Contact Type to collect:</strong><br />
  <select name="nimble_contact_type" id="nimble_contact_type">
  	<option value="0" <?php if(esc_attr(get_option('nimble_contact_type')) == 0) { echo " selected"; } ?>> People/Professionals</option>
    <option value="1" <?php if(esc_attr(get_option('nimble_contact_type')) == 1) { echo " selected"; } ?>> Companies</option>
  </select><br />
  
  <strong>Tags:</strong><br />
  <input type="text" class="span4" value="<?php echo esc_attr( get_option('nimble_tags') ); ?>" id="nimble_tags" name="nimble_tags" ><br />
  <div style="font-size: 9px;">Seperate tags with commas ','. (Currently only up to 5 tags can be transmitted but you can add as many as you like.)</div>
  <strong>Tags title:</strong><br />
  <input type="text" class="span4" value="<?php echo esc_attr( get_option('nimble_tags_title') ); ?>" id="nimble_tags_title" name="nimble_tags_title" ><br />
  <strong>Custom Fields</strong><br />
  <input type="text" class="span4" value="<?php echo esc_attr( get_option('nimble_customfields') ); ?>" id="nimble_customfields" name="nimble_customfields" ><br />
	<div style="font-size: 9px;">i.e. Github:text:Github,Stackoverflow:text:Stackoverflow,Remotely:checkbox:Want to work remotely. So far only text and checkbox are supported.</div>
  
  <strong>Include question for newsletter</strong><br />
  <select class="span4" name="nimble_newsletter">
    <option value="No" <?php if(esc_attr( get_option('nimble_newsletter') ) == "No" ) echo "selected"; ?>>No</option>
    <option value="Yes" <?php if(esc_attr( get_option('nimble_newsletter') ) == "Yes" ) echo "selected"; ?>>Yes</option>
  </select>
  <div style="font-size: 9px;">For Newsletter option to work you need to have a custom field in your account named 'Newsletter'</div>
  <strong>NewsLetter Title</strong><br />
  <input type="text" class="span4" value="<?php echo esc_attr( get_option('nimble_newsletter_title') ); ?>" id="nimble_newsletter_title" name="nimble_newsletter_title" ><br />
  
  <strong>Message upon success</strong><br />
  <input type="text" class="span4" value="<?php echo esc_attr( get_option('nimble_thanx') ); ?>" id="nimble_thanx" name="nimble_thanx" ><br />
  
  <input type="hidden" name="nimble_prefs" value="Y"> 
  <?php submit_button("Save Preferences"); ?>
  <br />
</form>